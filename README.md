# docker visualiseur

Dockerisation du projet tutoré à son stade fonctionnel.

## Démarrage :

* `docker-compose up -d`

## Autres infos :

* Il peut avoir un peu de temps pour que la base de donnée s'instancie, il faut donc rafraichir la page si un message d'erreur pdo/mysql arrive. La base de données est automatiquement crée et charger par un script sql au démarrage du container.
* Arrêter les containers : `docker-compose down`

### Les services :
* mariadb
* web
* phpmyadmin

| Service    | Ports | Container name | Volumes                                  |
| :--------- | :---- | :------------- | :--------------------------------------- |
| mariadb    | 3306  | mel-mariadb    | mariabd:/var/lib/mysql                   |
|            |       |                | ./sql-script:/docker-entrypoint-initdb.d |
| web        | 8080  | mel-web        | ./visualisateur:/var/www/html/           |
| phpmyadmin | 8741  | mel-phpmyadmin |                                          |