<?php
require_once "./public/src/models/Theme.php";
require_once "./public/src/models/Categorie.php";
require_once "./public/src/models/Object3D.php";
require_once "./public/src/models/UploadZipFile.php";
require_once "./public/src/models/ExtractZipFile.php";

class Object3DController{
    private Object3D $model;
    private Categorie $categorie;
    private Theme $theme;

    public function __construct()
    {
        $this->model = new Object3D();
        $this->categorie = new Categorie();
        $this->theme = new Theme();
    }

    public function listing()
    {
        $datas = array();
        $title = "Objets 3d";
        $page = "listing.php";
        $objectID = null;
        $administration = false;
        $alert = null;

        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            $page = "liste.php";
            $administration = true;

            if (isset($_GET['object']) && !is_null($_GET['object']) && !empty($_GET['object']) && is_numeric($_GET['object'])) {
                $page = "singlePageDashboard.php";
            }

            // Gestion de l'alert dans le dashboard
            if (isset($_SESSION['dashboard']['alert']) && !is_null($_SESSION['dashboard']['alert']) && !empty($_SESSION['dashboard']['alert'])) {
                $alert = $_SESSION['dashboard']['alert'];
                $_SESSION['dashboard']['alert'] = null;
            }
        }
        else {
            if (isset($_GET['object']) && !is_null($_GET['object']) && !empty($_GET['object']) && is_numeric($_GET['object'])) {
                $page = "singlePage.php";
            }
            else {
                header("location: http://" . getCurrentUrl() . "index.php?page=accueil");
            }
        }

        if (isset($_GET['object']) && !is_null($_GET['object']) && !empty($_GET['object']) && is_numeric($_GET['object'])) {
            $objectID = intval($_GET['object']);
            $object = $this->model->select($objectID)[0];
            $datas["object"] = $object;
            $datas["categories"] = $this->categorie->getCategorieByObject($objectID);
            $datas["themes"] = $this->theme->getThemeByObject($objectID);
            $datas["recommendeObject"] = $this->model->getSimilaire($objectID);
            $title .= " - " . $datas['object']['nom'];
            $this->model->updateVue($objectID, intval($object['vues']) + 1);
        }
        else {
            $datas = $this->model->select();
        }

        $vue = [
            "title" => $title,
            "page" => $page,
            "datas" => $datas,
            "administration" => $administration,
            "alert" => $alert
        ];

        return $vue;
    }

    public function create()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            return [
                "title" => "Créer - Object 3D",
                "page" => "create.php",
                "administration" => TRUE
            ];
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
        }
    }

    public function insert()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_POST['nom']) && !is_null($_POST['nom']) && !empty($_POST['nom'])) {
                date_default_timezone_set("Europe/Paris");

                $datas = [
                    "nom" => $_POST['nom'],
                    "description" => $_POST['description'],
                    "date_creation" => date("Y-m-d H:i:s"),
                ];

                // On insert les données dans la bdd et on récupère son id
                $slug = $this->model->insert($datas);

                // Destination du fichier upload
                $destination = "./public/assets/models3d/";

                // Upload :
                $zipUpload = new UploadZipFile("object", $_FILES, $slug, $destination);
                $zipUpload->upload();
                
                // Extract :
                $zipExtract = new ExtractZipFile;
                $zipExtract->extractZip($slug, $destination);
                $zipExtract->deleteZip($slug, $destination);

                $_SESSION["dashboard"]["alert"] = "Création du modèle 3D effectuée !";
            }

            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");

            return [
                "title" => "insertion...",
                "page" => "listing.php",
                "administration" => TRUE
            ];
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
        }
    }

    public function modifier()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            $datas = array();
            $title = "modifier - ";
            $page = "modifier.php";

            if (isset($_GET['object']) && !is_null($_GET['object']) && !empty($_GET['object']) && is_numeric($_GET['object'])) {
                $datas = $this->model->select(intval($_GET['object']))[0];
                $title .= $datas['nom'];
            }
            else {
                header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
            }

            return [
                "title" => $title,
                "page" => $page,
                "datas" => $datas,
                "administration" => TRUE
            ];
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
        }
    }

    public function update()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_POST['id']) && !is_null($_POST['id']) && !empty($_POST['id']) && is_numeric($_POST['id'])) {
                $datas = array(
                    "nom" => $_POST['nom'],
                    "description" => $_POST['description']
                );
    
                $this->model->update(intval($_POST['id']), $datas);
                
                if (isset($_FILES['file']['name']) && !is_null($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                    $destination = "./public/assets/models3d/";
                    $slug = $_POST['id'];

                    // Delete old folder
                    $zipExtract = new ExtractZipFile;
                    $zipExtract->deleteFolder($slug, $destination);
                    
                    // Upload :
                    $zipUpload = new UploadZipFile("file", $_FILES, $slug, $destination);
                    $zipUpload->upload();
                    
                    // // Extract :
                    $zipExtract->extractZip($slug, $destination);
                    $zipExtract->deleteZip($slug, $destination);
                }

                $_SESSION["dashboard"]["alert"] = "Modification du modèle 3D effectuée !";
            }
    
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
    
            return [
                "title" => "updating...",
                "page" => "listing.php",
                "administration" => TRUE
            ];
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
        }
        
    }

    public function delete()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_GET['object']) && !is_null($_GET['object']) && !empty($_GET['object']) && is_numeric($_GET['object'])) {
                $this->model->delete(intval($_GET['object']));

                $destination = "./public/assets/models3d/";
                $slug = $_GET['object'];

                // Delete old folder
                $zipExtract = new ExtractZipFile;
                $zipExtract->deleteFolder($slug, $destination);

                $_SESSION["dashboard"]["alert"] = "Suppression du modèle 3D effectuée !";
            }
    
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
    
            return [
                "title" => "deleting...",
                "page" => "listing.php",
                "administration" => TRUE
            ];
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=object-3d");
        }
    }

    public function search()
    {
        $datas = array();
        $title = "Résultats recherche";
        $page = "result.php";
        $datas["search"] = "Aucune recherche entrée !";
        $datas["object"] = null;

        if (isset($_POST['searchObject']) && !is_null($_POST['searchObject']) && !empty($_POST['searchObject'])) {
            $datas["search"] = $_POST['searchObject'];
            $datas["object"] = $this->model->search($datas["search"]);
        }

        $vue = [
            "title" => $title,
            "page" => $page,
            "datas" => $datas
        ];

        return $vue;
    }
}