<?php

require_once "./public/src/models/User.php";
class UtilisateurController{
    private User $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function listing()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") { 
        $datas = array();
        $title = "Liste des Utilisateurs";
        $page = "listing.php";

        if (isset($_GET['mail']) && !is_null($_GET['mail']) && !empty($_GET['mail'])) {
            $datas = $this->model->listing($_GET['mail'])[0];
            $page = "singlePage.php";
            $title = $datas['mail'];
        }
        else {
            $datas = $this->model->listing();
        }

        return [
            "title" => $title,
            "page" => $page,
            "datas" => $datas,
            "administration" => TRUE
        ];
        
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=user&action=connection");
    }

    public function modifier()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            
            $datas = array();
            $page = "modifier.php";
            $title = "modifier - ";

            if (isset($_GET['mail']) && !is_null($_GET['mail']) && !empty($_GET['mail'])) {
                $datas = $this->model->listing($_GET['mail'])[0];
                $title .= $datas['mail'];
            }

            return [
                "title" => $title,
                "page" => $page,
                "datas" => $datas,
                "administration" => TRUE
            ];
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=user");
    }

    public function update()
    {
        if (isset($_POST['mail']) && !is_null($_POST['mail']) & !empty($_POST['mail'])) {
            $mdp = password_hash(htmlentities($_POST['mdp']), PASSWORD_DEFAULT);
            
            $this->model->update($_POST['mail'], $_POST['role'], $mdp);
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=user");

        return [
            "title" => "updating...",
            "page" => "listing.php",
            "administration" => TRUE
        ];
    }

    public function create()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            
            return [
                "title" => "Créer Utilisateur",
                "page" => "create.php",
                "administration" => TRUE
            ];
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=user");
    }

    public function insert()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            
            if (isset($_POST['mail']) && !is_null($_POST['mail']) & !empty($_POST['mail'])) {
                $mdp = password_hash(htmlentities($_POST['mdp']), PASSWORD_DEFAULT);
                $date_creation = date("Y-m-d H:i:s");
                $this->model->add($_POST['mail'], $_POST['role'], $mdp, $date_creation);
            }

            header("location: http://" . getCurrentUrl() . "index.php?page=user");

            return [
                "title" => "adding...",
                "page" => "create.php",
                "administration" => TRUE
            ];
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=user");
    }

    public function delete()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            
            if (isset($_GET['mail']) && !is_null($_GET['mail']) & !empty($_GET['mail'])) {
                $this->model->remove($_GET['mail']);
            }
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=user");
    }

    public function connection()
    {
        return [
            "title" => "Login",
            "page" => "connection.php",
            "administration" => TRUE
        ];
    }

    public function login()
    {
        $message = "Veuillez vous connecter !";

        if (isset($_POST['mail']) && !is_null($_POST['mail']) & !empty($_POST['mail'])) {
            $mdp = htmlentities($_POST['mdp']);
            $exist = $this->model->Exist($_POST['mail']);

            if ($exist) {
                $user = $this->model->listing($_POST['mail'])[0];

                if (password_verify($mdp, $user['mdp'])) {
                    $_SESSION['utilisateur'] = array(
                        "mail" => $_POST['mail'],
                        "role" => $user['role']
                    );
                    $message = "Connection effectuée !";
                }
                else {
                    $message = "Erreure de login !";
                }
            }
            else {
                $message = "Erreure de login !";
            }
        }

        return [
            "title" => "Login",
            "page" => "login.php",
            "message" => $message,
            "administration" => TRUE
        ];
    }

    public function logout()
    {
        session_reset();
        session_unset();
        session_destroy();

        return [
            "title" => "Logout",
            "page" => "logout.php",
            "administration" => TRUE
        ];
    }
}