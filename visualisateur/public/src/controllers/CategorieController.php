<?php

require_once "./public/src/models/Categorie.php";
require_once "./public/src/models/Object3D.php";
require_once "./public/src/models/Theme.php";
require_once "./public/src/models/relationModel.php"; 

class CategorieController{
    private Categorie $model;
    private Object3D $object;
    private Theme $theme;
    private RelationModel $relation;

    public function __construct()
    {
        $this->model = new Categorie();
        $this->object = new Object3D();
        $this->theme = new Theme();
        $this->relation = new RelationModel();
    }

    public function listing()
    {
        $datas = array();
        $page = "listing.php";
        $title = "Liste des catégories";
        $categorieID = null;
        $administration = false;
        $alert = null;

        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_GET['categorie']) && !is_null($_GET['categorie']) && !empty($_GET['categorie']) && is_numeric($_GET['categorie'])) {
                $categorieID = intval($_GET['categorie']);
                $datas['categorie'] = $this->model->select($categorieID)[0];
                $datas['themes'] = $this->theme->getThemeByCategorie($categorieID);
                $datas['parents'] = $this->model->getParentCategorie($categorieID);
                $datas['child'] = $this->model->getChildCategorie($categorieID);
                $page = "singlePageDashboard.php";
            }
            else {
                $datas['categorie'] = $this->model->select();
                $page = "liste.php";
            }
            $administration = true;

            // Gestion de l'alert dans le dashboard
            if (isset($_SESSION['dashboard']['alert']) && !is_null($_SESSION['dashboard']['alert']) && !empty($_SESSION['dashboard']['alert'])) {
                $alert = $_SESSION['dashboard']['alert'];
                $_SESSION['dashboard']['alert'] = null;
            }
        }
        else {
            if (isset($_GET['categorie']) && !is_null($_GET['categorie']) && !empty($_GET['categorie']) && is_numeric($_GET['categorie'])) {
                $categorieID = intval($_GET['categorie']);
                $datas['categorie'] = $this->model->select($categorieID)[0];
                $datas['object'] = $this->object->getObjectByCategorie($categorieID);
                $datas['parents'] = $this->model->getParentCategorie($categorieID);
                $datas['child'] = $this->model->getChildCategorie($categorieID);
                $page = "singlePage.php";
            }
            else {
                header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
            }
        }

        $vue = [
            "title" => $title,
            "page" => $page,
            "datas" => $datas,
            "administration" => $administration,
            "alert" => $alert
        ];

        return $vue;
    }

    public function create()
    {

        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            $page = "create.php";
            $title = "Créer - Catégorie";
            $datas = array(
                "categorie" => $this->model->select(),
                "object" => $this->object->select()
            );
            
            $vue = [
                "title" => $title,
                "page" => $page,
                "administration" => TRUE,
                "datas" => $datas
            ];

            return $vue;
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=categorie");
        }
    }

    public function insert()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_POST['nom']) && !is_null($_POST['nom']) && !empty($_POST['nom'])) {
                $nom = $_POST['nom'];
                $description = $_POST['description'];
                date_default_timezone_set("Europe/Paris");
                $date_creation = date("Y-m-d H:i:s");

                $datas = array(
                    "nom" => $nom,
                    "description" => $description,
                    "date_creation" => $date_creation
                );

                $this->model->insert($datas);
                $newCategorie = $this->model->selectWithParams($datas)[0];

                if (isset($_POST['objectRelation']) && !empty($_POST['objectRelation']) && !is_null($_POST['objectRelation'])) {
                    $this->relation->setCategorieObject(intval($newCategorie['id']), $_POST['objectRelation']);
                }
                if (isset($_POST['categorieRelation']) && !empty($_POST['categorieRelation']) && !is_null($_POST['categorieRelation'])) {
                    $this->relation->setCategorieCategorie(intval($newCategorie['id']), $_POST['categorieRelation']);
                }
                $_SESSION["dashboard"]["alert"] = "Création de la catégorie effectuée !";
            }
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=categorie");
        
        return [
            "title" => "Insertion...",
            "page" => "listing.php",
            "administration" => TRUE
        ];
    }

    public function modifier()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            $datas = array();
            $title = "Modifier - ";
            $page = "listing.php";


            if (isset($_GET['categorie']) && !is_null($_GET['categorie']) && !empty($_GET['categorie']) && is_numeric($_GET['categorie'])) {

                $datas['categorie'] = $this->model->select(intval($_GET['categorie']))[0];
                $datas['categories'] = $this->model->select();
                $datas['object'] = $this->object->select();
                $datas['objectEnfant'] = $this->object->getChildObject(intval($_GET['categorie']));
                $datas['enfant'] = $this->model->getChildCategorie(intval($_GET['categorie']));

                foreach ($datas['categories'] as $key => $categorie) {
                    foreach ($datas['enfant'] as $enfant) {
                        if ($categorie['id'] == $enfant['idCategorie']) {
                            unset($datas['categories'][$key]);
                        }
                    }
                }

                foreach ($datas['object'] as $key => $object) {
                    foreach ($datas['objectEnfant'] as $enfant) {
                        if ($object['id'] == $enfant['id']) {
                            unset($datas['object'][$key]);
                        }
                    }
                }

                $title .= $datas['categorie']['nom'];
                $page = "modifier.php";
            }
            else {
                header("location: http://" . getCurrentUrl() . "index.php?page=categorie");
            }
            
            return [
                "title" => $title,
                "page" => $page,
                "datas" => $datas,
                "administration" => TRUE
            ];
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=categorie");
        }
    }

    public function update()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) &&$_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_POST['id']) && !is_null($_POST['id']) && !empty($_POST['id']) && is_numeric($_POST['id'])) {
                $id = intval($_POST['id']);
                $nom = $_POST['nom'];
                $description = $_POST['description'];
                $datas = array(
                    "nom" => $nom,
                    "description" => $description
                );

                $this->model->update($id, $datas);
                if (isset($_POST['objectRelation']) && !empty($_POST['objectRelation']) && !is_null($_POST['objectRelation'])) {
                    $this->relation->setCategorieObject(intval($_POST['id']), $_POST['objectRelation']);
                }
                if (isset($_POST['categorieRelation']) && !empty($_POST['categorieRelation']) && !is_null($_POST['categorieRelation'])) {
                    $this->relation->setCategorieCategorie(intval($_POST['id']), $_POST['categorieRelation']);
                }
                $_SESSION["dashboard"]["alert"] = "Modification de la catégorie effectuée !";
            }

            header("location: http://" . getCurrentUrl() . "index.php?page=categorie");
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=categorie");

        return [
            "title" => "updating...",
            "page" => "listing.php",
            "administration" => TRUE
        ];
    }

    public function delete()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_GET['categorie']) && !is_null($_GET['categorie']) && !empty($_GET['categorie']) && is_numeric($_GET['categorie'])) {
                $this->model->delete(intval($_GET['categorie']));

                $_SESSION["dashboard"]["alert"] = "Suppression de la catégorie effectuée !";
            }
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=categorie");

        return [
            "title" => "deleting...",
            "page" => "listing.php",
            "administration" => TRUE
        ];
    }
}