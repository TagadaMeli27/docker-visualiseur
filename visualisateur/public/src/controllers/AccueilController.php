<?php

require_once "./public/src/models/Object3D.php";
require_once "./public/src/models/Theme.php";

class AccueilController{
    private Object3D $object;
    private Theme $theme;

    public function __construct()
    {
        $this->object = new Object3D();
        $this->theme = new Theme();
    }

    public function listing()
    {
        $objects = $this->object->getObjectAcceuil();
        $themes = $this->theme->getThemeAcceuil();
        $recommende = $this->object->getObjectRecommende();

        $vue = array(
            "title" => "Accueil",
            "page" => "listing.php",
            "datas" => array(
                "object" => $objects,
                "theme" => $themes,
                "recommendeObject" => $recommende
            )
        );

        return $vue;
    }

    public function administration()
    {
        if (isset($_SESSION['utilisateur']) && !is_null($_SESSION['utilisateur']) && !empty($_SESSION['utilisateur']) && $_SESSION['utilisateur']['role'] == "admin") {
            $vue = array(
                "title" => "Tableau de board",
                "page" => "dashboard.php",
                "administration" => TRUE
            );

            return $vue;
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=accueil");
        }
    }
}