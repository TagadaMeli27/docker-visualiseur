<?php

require_once "./public/src/models/Theme.php";
require_once "./public/src/models/Categorie.php";
require_once "./public/src/models/Object3D.php";
require_once "./public/src/models/relationModel.php";

class ThemeController{
    private $theme = null;
    private $categorie = null;
    private $object = null;
    private $relation = null;

    public function __construct()
    {
        $this->theme = new Theme();
        $this->categorie = new Categorie();
        $this->object = new Object3D();
        $this->relation = new RelationModel();
    }

    public function listing()
    {
        $datas = array();
        $vue = array();
        $page = "listing.php";
        $title = "Thèmes 3D";
        $themeID = null;
        $administration = false;

        if (isset($_GET['theme']) && !is_null($_GET['theme']) && !empty($_GET['theme']) && is_numeric($_GET['theme'])) {
            $themeID = intval($_GET['theme']);
        }

        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role'])) {
            $page = "liste.php";
            $administration = true;
            if (!is_null($themeID)) {
                $page = "singlePageDashboard.php";
            }

            // Gestion de l'alert dans le dashboard
            if (isset($_SESSION['dashboard']['alert']) && !is_null($_SESSION['dashboard']['alert']) && !empty($_SESSION['dashboard']['alert'])) {
                $vue["alert"] = $_SESSION['dashboard']['alert'];
                $_SESSION['dashboard']['alert'] = null;
            }
        }
        else {
            if (!is_null($themeID)) {
                $page = "singlePage.php";
            }
        }

        if (is_null($themeID)) {
            $datas = $this->theme->select();
        }
        else {
            $datas['theme'] = $this->theme->select($themeID)[0];
            $datas['object'] = $this->object->getObjectByTheme($themeID);
            $datas['categorie'] = $this->categorie->getCategorieByTheme($themeID);

            $_SESSION['pathTheme'] = array(
                "themeNom" => $datas['theme']['nom'],
                "themeId" => $datas['theme']['id']
            );
        }

        $vue['title'] = $title;
        $vue['datas'] = $datas;
        $vue['page'] = $page;
        $vue['administration'] = $administration;
        
        return $vue;
    }

    public function modifier()
    {
        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role']) && $_SESSION['utilisateur']['role'] == "admin") {
            $datas = array();
            $title = "Modifier - ";
            $themeID = null;

            if (isset($_GET['theme']) && !is_null($_GET['theme']) && !empty($_GET['theme']) && is_numeric($_GET['theme'])) {
                $themeID = intval($_GET['theme']);
            }
            else {
                header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
            }
            $datas['theme'] = $this->theme->select($themeID)[0];
            $datas['categorie'] = $this->categorie->select();
            $datas['categorieEnfant'] = $this->theme->getCategorie($themeID);


            foreach ($datas['categorie'] as $key =>$parent) {
                foreach ($datas['categorieEnfant'] as $enfant) {
                    if ($enfant['id'] == $parent['id']) {
                        unset($datas['categorie'][$key]);
                    }
                }
            }

            $vue['title'] = $title .= $datas['theme']['nom'];
            $vue['datas'] = $datas;
            $vue['page'] = "modifier.php";
            $vue['administration'] = true;

            return $vue;
        }
        else {
            header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
        }
    }

    public function update()
    {
        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role']) && $_SESSION['utilisateur']['role'] == "admin") {
            $datas = array();
            $title = "updating";
    
            if (isset($_POST['id']) && !is_null($_POST['id']) && !empty($_POST['id']) && is_numeric($_POST['id'])) {
    
                $themeID = intval($_POST['id']);
                $nom = $_POST['nom'];
                $description = $_POST['description'];
    
                $datas = array(
                    "nom" => $nom,
                    "description" => $description
                );
    
                $this->theme->update($themeID, $datas);
                if (isset($_POST['categorieRelation']) && !is_null($_POST['categorieRelation']) && !empty($_POST['categorieRelation'])) {
                    $this->relation->setThemeCategorie(intval($themeID), $_POST['categorieRelation']);
                }
                $_SESSION["dashboard"]["alert"] = "Modification du thème effectuée !";
            }

            header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
    
            return [
                "title" => $title,
                "page" => "listing.php",
                "administration" => TRUE
            ];
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
        
    }

    public function delete()
    {
        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_GET['theme']) && !is_null($_GET['theme']) && !empty($_GET['theme']) && is_numeric($_GET['theme'])) {
                $themeID = intval($_GET['theme']);
                $this->theme->delete($themeID);

                $_SESSION["dashboard"]["alert"] = "Suppression du thème effectuée !";
            }

            header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");

            return [
                "title" => "deleting",
                "page" => "listing.php",
                "administration" => TRUE
            ];
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
    }

    public function create()
    {
        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role']) && $_SESSION['utilisateur']['role'] == "admin") {
            $datas = array(
                "categorie" => $this->categorie->select()
            );

            $vue = array(
                "title" => "Créer - Thèmes 3D",
                "page" => "create.php",
                "administration" => TRUE,
                "datas" => $datas
            );

            return $vue;
        }

        header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
    }

    public function insert(){
        if (isset($_SESSION['utilisateur']['role']) && !is_null($_SESSION['utilisateur']['role']) && !empty($_SESSION['utilisateur']['role']) && $_SESSION['utilisateur']['role'] == "admin") {
            if (isset($_POST['nom']) && !is_null($_POST['nom']) && !empty($_POST['nom'])) {
                $nom = $_POST['nom'];
                $description = $_POST['description'];
                date_default_timezone_set("Europe/Paris");
                $date_creation = date("Y-m-d H:i:s");
    
                $datas = array(
                    "nom" => $nom,
                    "description" => $description,
                    "date_creation" => $date_creation
                );
    
                $this->theme->insert($datas);
                $theme = $this->theme->selectWithParams($datas)[0];

                if (isset($_POST['categorieRelation']) && !is_null($_POST['categorieRelation']) && !empty($_POST['categorieRelation'])) {
                    $this->relation->setThemeCategorie(intval($theme['id']), $_POST['categorieRelation']);
                }
                
                $_SESSION["dashboard"]["alert"] = "Création du thème effectuée !";
            }
    
            header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
    
            return [
                "title" => "Thèmes 3D",
                "page" => "listing.php",
                "administration" => TRUE
            ];
        }
        
        header("location: http://" . getCurrentUrl() . "index.php?page=theme-3d");
    }
}