<?php
require_once "./public/src/models/Mail.php";

class ContactController
{
    private Mail $mail;

    public function __construct()
    {
        $this->mail = new Mail(""); // Attention à bien mettre l'adresse mail où recevoir le formulaire de contact

    }

    public function listing()
    {
        $alert = false;
        if (isset($_POST["mail"]) && !empty($_POST["mail"]) && !is_null($_POST["mail"]) 
            && isset($_POST["object"]) && !empty($_POST["object"]) && !is_null($_POST["object"])
            && isset($_POST["message"]) && !empty($_POST["message"]) && !is_null($_POST["message"]))
        {
            $this->mail->send($_POST["mail"], $_POST["object"], $_POST["message"]);

            $alert = $this->mail->debug();
        }

        $vue = array(
            "title" => "Contact",
            "page" => "listing.php",
            "alert" => $alert
        );

        return $vue;
    }
}