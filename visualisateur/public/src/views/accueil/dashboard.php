<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="connection"><?= $_SESSION["utilisateur"]["role"] ?></li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="?page=accueil&action=administration">Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=theme-3d&action=listing">Thèmes 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=categorie&action=listing">Catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=object-3d&action=listing">Modèles 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=user&action=listing">Comptes</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Tableau de bord</h1>
            </div>
            <div class="row justify-content-center">
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Thèmes 3D</h3>
                    <div class="btn-group">
                        <a class="btn btn-success" href="?page=theme-3d&action=create">Ajouter</a>
                        <a class="btn btn-outline-primary" href="?page=theme-3d&action=listing">Consulter</a>
                    </div>
                </section>
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Catégories</h3>
                    <div class="btn-group">
                        <a class="btn btn-success" href="?page=categorie&action=create">Ajouter</a>
                        <a class="btn btn-outline-primary" href="?page=categorie&action=listing">Consulter</a>
                    </div>
                </section>
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Modèle 3D</h3>
                    <div class="btn-group">
                        <a class="btn btn-success" href="?page=object-3d&action=create">Ajouter</a>
                        <a class="btn btn-outline-primary" href="?page=object-3d&action=listing">Consulter</a>
                    </div>
                </section>
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Comptes</h3>
                    <a class="btn btn-outline-primary" href="?page=user&action=listing">Consulter</a>
                </section>
            </div>
        </main>
    </div>
</div>