<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">Accueil</li>
        </ol>
    </nav>

    <section>
        <h2>Nouveautés</h2>
        <ul class="carousel">
        <!-- Ici placer les objets "nouveaux" -->
        <?php foreach($vue['datas']['object'] as $object) { ?>
            <li class="carousel-item">
                <a href="?page=object-3d&object=<?= $object['id'] ?>">
                <div class="card bg-dark text-white">
                    <img class="card-img vignette" width="300" height="200" src="./public/assets/models3d/<?= $object["id"] ?>/vignette.png" alt="vignette">
                    <div class="card-img-overlay bg-dark opacity-25"></div>
                    <div class="card-img-overlay d-flex flex-column justify-content-end align-items-center">
                        <h3 class="card-title"><?= $object['nom'] ?></h5>
                    </div>
                </div>
                </a>
            </li>
        <?php } ?>
        <!-- Fin de l'affichage des objets "nouveaux" -->
        </ul>
    </section>

    <section>
        <h2>Thème</h2>
        <ul class="carousel">
        <!-- Ici placer les objets "theme" -->
        <?php foreach($vue['datas']['theme'] as $theme) { ?>
            <li class="carousel-item">
                <a href="?page=theme-3d&theme=<?= $theme['id'] ?>">
                <div class="card bg-dark text-white">
                    <div class="vignette bg-primary"></div>
                    <div class="card-img-overlay bg-dark opacity-25"></div>
                    <div class="card-img-overlay d-flex flex-column justify-content-end align-items-center">
                        <h5 class="card-title"><?= $theme['nom'] ?></h5>
                        <p class="card-text text-center"><?= $theme['description'] ?></p>
                    </div>
                </div>
                </a>
            </li>
        <?php } ?>
        <!-- Fin de l'affichage des objets "theme" -->
        </ul>
    </section>

    <section>
        <h2>Recommandé</h2>
        <ul class="carousel">
        <!-- Ici placer les objets "recommandé" -->
        <?php foreach($vue['datas']['recommendeObject'] as $object) { ?>
            <li class="carousel-item">
                <a href="?page=object-3d&object=<?= $object['id'] ?>">
                <div class="card bg-dark text-white">
                    <img class="card-img vignette" width="300" height="200" src="./public/assets/models3d/<?= $object["id"] ?>/vignette.png" alt="vignette">
                    <div class="card-img-overlay bg-dark opacity-25"></div>
                    <div class="card-img-overlay d-flex flex-column justify-content-end align-items-center">
                        <h5 class="card-title"><?= $object['nom'] ?></h5>
                    </div>
                </div>
                </a>
            </li>
        <?php } ?>
        <!-- Fin de l'affichage des objets "recommandé" -->
        </ul>
    </section>
    <!--<section>
        <h2>Déjà vu</h2>
        <ul class="carousel">
        Ici placer les objets "dejavu" 
        <?php
        // for ($i = 0; $i < 10; $i++)
        // {
            ?>
            <li class="carousel-item">
                <div class="card bg-dark text-white">
                    <img class="card-img responsive-img" width="300" height="200" src="http://via.placeholder.com/300x200" alt="">
                    <div class="card-img-overlay bg-dark opacity-25"></div>
                    <div class="card-img-overlay d-flex flex-column justify-content-end align-items-center">
                        <h3 class="card-title">Model name <?= $i ?></h5>
                        <p class="card-text">Short description of the model.</p>
                    </div>
                </div>
            </li>
            <?php
        // } 
        ?>
        Fin de l'affichage des objets "dejavu"
        </ul>
    </section>-->
</div>