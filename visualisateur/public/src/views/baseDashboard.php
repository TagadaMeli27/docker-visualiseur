<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $vue['title'] ?></title>
    <link rel="stylesheet" href="./public/assets/css/dashboard.css">
</head>
<body>
<div class="bg-light">
    <header class="navbar bg-background sticky-top flex-md-nowrap p-0 box-shadow"><?php require_once "./public/src/views/headerDashboard.php"; ?></header>
    <?php require_once "./public/src/views/$page/" . $vue['page']; ?>
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="./public/assets/js/index.js"></script>
</body>
</html>