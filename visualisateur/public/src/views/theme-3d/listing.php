<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">Thème 3D</li>
        </ol>
    </nav>
</div>
<nav class="mb-3">
    <ul class="sub-nav-1 sub-nav-sm-2 sub-nav-md-3 nav-pills">
        <!-- Ici placer les thèmes -->
        <?php foreach ($vue['datas'] as $themes) { ?>
        <li class="nav-item">
            <a class="nav-link" href="?page=theme-3d&theme=<?= $themes['id'] ?>"><?= ucfirst($themes['nom']) ?></a>
        </li>
        <?php } ?>
        <!-- Fin de l'affichage des thèmes -->
    </ul>
</nav>