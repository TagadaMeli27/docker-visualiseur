<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="connection"><?= $_SESSION["utilisateur"]["role"] ?></li>
                    <li class="nav-item">
                        <a class="nav-link"href="?page=accueil&action=administration">Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="?page=theme-3d&action=listing">Thèmes 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=categorie&action=listing">Catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=object-3d&action=listing">Modèles 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=user&action=listing">Comptes</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Thèmes 3D - <?= ucfirst($vue['datas']['theme']['nom']) ?></h1>
            </div>
            <ul class="list-group">
                <li class="list-group-item list-group-item-action"><span class="badge bg-primary">id :</span> <?= $vue['datas']['theme']['id'] ?></li>
                <li class="list-group-item list-group-item-action"><span class="badge bg-primary">Nom :</span> <?= $vue['datas']['theme']['nom'] ?></li>
                <li class="list-group-item list-group-item-action"><span class="badge bg-primary">Description :</span> <?= $vue['datas']['theme']['description'] ?></li>
                <li class="list-group-item list-group-item-action"><span class="badge bg-primary">Date de création :</span> <?= $vue['datas']['theme']['date_creation'] ?></li>
            </ul>
            <ul class="list-group">
                <li class="list-group-item list-group-item-action">Catégories enfants : 
                    <?php foreach ($vue['datas']['categorie'] as $categorie) { ?>
                        <span class="badge bg-success"><?= $categorie['nomCategorie'] ?></span>
                    <?php } ?>
                </li>
            </ul>
        </main>
    </div>
</div>