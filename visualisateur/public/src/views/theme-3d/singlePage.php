<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="?page=theme-3d">Thème 3D</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?= ucfirst($vue['datas']['theme']['nom']) ?></li>
        </ol>
    </nav>
    <h1 class="text-center"><?= ucfirst($vue['datas']['theme']['nom']) ?></h1>
</div>
<nav class="mb-3">
    <ul class="sub-nav-1 sub-nav-sm-2 sub-nav-md-3 nav-pills">
        <!-- Ici placer les catégories du thème -->
        <?php foreach($vue['datas']['categorie'] as $categorie) { ?>
            <li class="nav-item">
                <a class="nav-link" href="?page=categorie&categorie=<?= $categorie['idCategorie'] ?>"><?= $categorie['nomCategorie'] ?></a>
            </li>
        <?php } ?>
        <!-- Fin de l'affichage des catégories du thème -->
    </ul>
</nav>
<div class="container-md mb-3">
    <article class="d-flex flex-wrap justify-content-center">
        <!-- Ici placer tous les models du thème -->
        <?php foreach ($vue['datas']['object'] as $object) { ?>
            <a href="?page=object-3d&action=listing&object=<?= $object['idObject'] ?>" class="display-contents text-decoration-none">
                <section class="card fit-content m-1">
                    <img class="card-img-top vignette" width="300" height="200" src="./public/assets/models3d/<?= $object['idObject'] ?>/vignette.png" alt="vignette">
                    <div class="card-footer bg-primary text-white">
                        <h5 class="card-title m-0"><?= $object['nomObject'] ?></h5>
                    </div>
                </section>
            </a>
        <?php } ?>
        <!-- Fin de l'affichage des models du thème -->
    </article>
</div>