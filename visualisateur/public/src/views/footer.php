<div class="container-md">
    <div class="row py-3">
        <section class="col-md-6">
            <h3>Crédits</h3>
            <p>Conception et réalisation du site par les étudiants de l'IUT d'Orléans dans le cadre du projet tutoré de la Licence Professionnelle Conception Développment et Tests de Logiciels 2021/2022.</p>
            <p>Bouazza Miloud, Leray Mélissa, Tertereau Rhayan</p>
        </section>
        <section class="col-md-6">
            <h3>Autres informations footer</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita eos, ducimus repellat cupiditate autem, voluptatibus iste natus, quaerat animi ratione similique fugit veritatis temporibus nobis suscipit! Amet soluta deleniti sint.</p>
        </section>
    </div>
</div>