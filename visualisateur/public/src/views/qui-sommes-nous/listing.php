<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">Qui sommes-nous ?</li>
        </ol>
    </nav>
</div>
<nav class="mb-3">
    <ul class="sub-nav-1 sub-nav-sm-2 sub-nav-md-3 nav-pills">
        <li class="nav-item">
            <a class="nav-link" href="#historique">Historique</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#service">Service</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#partenaires">Partenaires</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#equipe">Equipe</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#materiel">Materiel</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#video">Vidéo</a>
        </li>
    </ul>
</nav>
<div class="container-md">
    <article class="mb-5">
        <h1 class="text-center">Qui sommes-nous ?</h1>
        <section id="historique" class="responsive-ancre">
            <h2>Historique</h2>
            <div class="ps-3">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit a maiores, dignissimos aliquid voluptas perspiciatis nihil deleniti id enim alias voluptate autem rem fuga incidunt temporibus aspernatur inventore non tempora?</p>
            </div>
        </section>
        <section id="service" class="responsive-ancre">
            <h2>Service</h2>
            <div class="ps-3">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit a maiores, dignissimos aliquid voluptas perspiciatis nihil deleniti id enim alias voluptate autem rem fuga incidunt temporibus aspernatur inventore non tempora?</p>
            </div>
        </section>
        <section id="partenaires" class="responsive-ancre">
            <h2>Partenaires</h2>
            <div class="ps-3">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit a maiores, dignissimos aliquid voluptas perspiciatis nihil deleniti id enim alias voluptate autem rem fuga incidunt temporibus aspernatur inventore non tempora?</p>
            </div>
        </section>
        <section id="equipe" class="responsive-ancre">
            <h2>Equipe</h2>
            <div class="ps-3">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit a maiores, dignissimos aliquid voluptas perspiciatis nihil deleniti id enim alias voluptate autem rem fuga incidunt temporibus aspernatur inventore non tempora?</p>
            </div>
        </section>
        <section id="materiel" class="responsive-ancre">
            <h2>Materiel</h2>
            <div class="ps-3">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit a maiores, dignissimos aliquid voluptas perspiciatis nihil deleniti id enim alias voluptate autem rem fuga incidunt temporibus aspernatur inventore non tempora?</p>
            </div>
        </section>
        <section id="video" class="responsive-ancre">
            <h2>Vidéo</h2>
            <div class="ps-3">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit a maiores, dignissimos aliquid voluptas perspiciatis nihil deleniti id enim alias voluptate autem rem fuga incidunt temporibus aspernatur inventore non tempora?</p>
            </div>
        </section>
    </article>
</div>