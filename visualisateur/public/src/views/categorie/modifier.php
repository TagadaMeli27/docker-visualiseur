<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="connection"><?= $_SESSION["utilisateur"]["role"] ?></li>
                    <li class="nav-item">
                        <a class="nav-link"href="?page=accueil&action=administration">Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=theme-3d&action=listing">Thèmes 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page"  href="?page=categorie&action=listing">Catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=object-3d&action=listing">Modèles 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=user&action=listing">Comptes</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Catégories - Modifications</h1>
            </div>
            <form action="?page=categorie&action=update" method="POST">
                <div class="mb-3">
                    <label class="form-label" for="nom">Nom de la catégorie :</label>
                    <input class="form-control" type="text" name="nom" id="nom" value="<?= $vue['datas']['categorie']['nom'] ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="categorieRelation">Catégorie enfants :</label>
                    <select class="form-select" name="categorieRelation[]" id="categorieRelation" multiple size="5">
                        <?php foreach($vue['datas']['enfant'] as $categorie){ ?>
                            <option value="<?= $categorie['idCategorie'] ?>" selected><?= $categorie['nomCategorie'] ?></option>
                        <?php } ?>
                        <?php foreach($vue['datas']['categories'] as $categorie){ ?>
                            <option value="<?= $categorie['id'] ?>"><?= $categorie['nom'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="objectRelation">Objets enfants :</label>
                    <select class="form-select" name="objectRelation[]" id="objectRelation" multiple size="8">
                        <?php foreach($vue['datas']['objectEnfant'] as $object){ ?>
                            <option value="<?= $object['id'] ?>" selected><?= $object['nom'] ?></option>
                        <?php } ?>
                        <?php foreach($vue['datas']['object'] as $object){ ?>
                            <option value="<?= $object['id'] ?>"><?= $object['nom'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="description">Description de la catégorie :</label>
                    <textarea class="form-control" name="description" id="description" cols="30" rows="5"><?= $vue['datas']['categorie']['description'] ?></textarea>
                </div>
                <input type="hidden" name="id" value="<?= $vue['datas']['categorie']['id'] ?>">

                <input class="btn btn-success" type="submit" value="Enregistrer">
            </form>
        </main>
    </div>
</div>