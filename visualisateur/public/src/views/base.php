<?php

$url = getCurrentUrl();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $vue['title'] ?></title>
    <link rel="stylesheet" href="./public/assets/css/main.css">
    <!-- <link rel="stylesheet" href="./public/assets/css/<?= $page ?>.css"> -->
</head>
<body>

    <header class="sticky-top bg-light box-shadow"><?php require_once "./public/src/views/header.php"; ?></header>
    <main><?php require_once "./public/src/views/$page/" . $vue['page']; ?></main>
    <footer class="bg-background"><?php require_once "./public/src/views/footer.php"; ?></footer>

    <!-- Bootstrap js -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- Custom js -->
    <script src="./public/assets/js/index.js"></script>
    <!-- <script src="./public/assets/js/<?= $page ?>.js"></script> -->
</body>
</html>