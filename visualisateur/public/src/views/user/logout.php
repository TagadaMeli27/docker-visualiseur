<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="nav-item mb-3">
                        <a class="nav-link btn btn-primary" href="?page=user&action=connection">Connection</a>
                    </li>
                    <li class="nav-item mb-3">
                        <a class="nav-link btn btn-primary" href="?page=accueil">Page d'accueil</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Vous êtes désormais déconnectés</h1>
            </div>
        </main>
    </div>
</div>