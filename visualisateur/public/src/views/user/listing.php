<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="connection"><?= $_SESSION["utilisateur"]["role"] ?></li>
                    <li class="nav-item">
                        <a class="nav-link"href="?page=accueil&action=administration">Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=theme-3d&action=listing">Thèmes 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"  href="?page=categorie&action=listing">Catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=object-3d&action=listing">Modèles 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="?page=user&action=listing">Comptes</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Comptes</h1>
            </div>
            <div class="row justify-content-center">
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Ajouter un utilisateur</h3>
                    <div class="btn-group">
                        <a class="btn btn-success" href="?page=user&action=create">Ajouter</a>
                    </div>
                </section>
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Modifier vos informations</h3>
                    <div class="btn-group">
                        <a class="btn btn-warning" href="?page=user&action=modifier&mail=<?= $_SESSION['utilisateur']['mail'] ?>">Modifier</a>
                    </div>
                </section>
                <section class="form-admin d-flex flex-column justify-content-around align-items-center">
                    <h3>Consulter vos informations</h3>
                    <div class="btn-group">
                        <a class="btn btn-primary" href="?page=user&mail=<?= $_SESSION['utilisateur']['mail'] ?>">Consulter</a>
                    </div>
                </section>
            </div>
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <p>Seul un membre ayant un rôle "admin" peut accéder à ce back office !</p>
            </div>
            <div class="d-flex justify-content-end align-items-center mb-3">
                <span><?= count($vue['datas']) ?> résultats</span>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-hover"> 
                    <thead class="bg-background">
                        <tr>
                            <th class="text-no-wrap" scope="col">#</th>
                            <th class="text-no-wrap" scope="col">Mail</th>
                            <th class="text-no-wrap" scope="col">Rôle</th>
                            <th class="text-no-wrap" scope="col">Date de création</th>
                            <th class="text-no-wrap" scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index=1; foreach ($vue['datas'] as $user) { ?>
                            <tr>
                                <th scope="row"><?= $index++ ?></th>
                                <td><?= $user['mail'] ?></td>
                                <td><?= $user['role'] ?></td>
                                <td><?= $user['date_creation'] ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger" href="?page=user&action=delete&mail=<?= $user['mail'] ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                                            </svg>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>