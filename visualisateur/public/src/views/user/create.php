
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="connection"><?= $_SESSION["utilisateur"]["role"] ?></li>
                    <li class="nav-item">
                        <a class="nav-link"href="?page=accueil&action=administration">Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=theme-3d&action=listing">Thèmes 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=categorie&action=listing">Catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=object-3d&action=listing">Modèles 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="?page=user&action=listing">Comptes</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Comptes - Créer</h1>
            </div>
            <form action="?page=user&action=insert" method="POST">
                <div class="mb-3">
                    <label class="form-label" for="mail">Email :</label>
                    <input class="form-control" type="email" name="mail" id="mail" required>
                </div>
                <div class="mb-3">
                    <label for="role" class="form-label">Role :</label>
                    <input class="form-control" type="text" name="role" id="role" required>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="mdp">Mot de passe :</label>
                    <input class="form-control" type="password" name="mdp" id="mdp" required>
                </div>

                <input class="btn btn-success" type="submit" value="Enregistrer">
            </form>
        </main>
    </div>
</div>
