<div class="d-flex justify-content-center mt-5">
    <form class="text-center" action="./index.php?page=user&action=login" method="POST">
        <div class="mb-3 text-start">
            <label class="form-label" for="mail">Email :</label>
            <input class="form-control" type="mail" name="mail" id="mail">
        </div>
        <div class="mb-3 text-start">
            <label class="form-label" for="mdp">Mot de passe :</label>
            <input class="form-control" type="password" name="mdp" id="mdp">
        </div>

        <input class="btn btn-success w-100" type="submit" value="Connection">
    </form>
</div>