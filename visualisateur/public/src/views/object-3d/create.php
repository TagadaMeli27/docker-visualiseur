<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="bg-darken col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="pt-2">
                <ul class="nav flex-column">
                    <li class="connection"><?= $_SESSION["utilisateur"]["role"] ?></li>
                    <li class="nav-item">
                        <a class="nav-link"href="?page=accueil&action=administration">Tableau de bord</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=theme-3d&action=listing">Thèmes 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=categorie&action=listing">Catégories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="?page=object-3d&action=listing">Modèles 3D</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=user&action=listing">Comptes</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex flex-column align-items-center p-2 mb-3 title">
                <h1>Modèles 3D - Créer</h1>
            </div>
            <form action="?page=object-3d&action=insert" method="POST" enctype="multipart/form-data">
                <div class="mb-3">
                    <label class="form-label" for="nom">Nom de l'objet :</label>
                    <input class="form-control" type="text" name="nom" id="nom" required>
                </div>
                <div class="mb-3">
                    <label for="file" class="form-label">Archive zip de l'objet :</label>
                    <input class="form-control" type="file" name="object" id="file" required>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="description">Description de l'objet :</label>
                    <textarea class="form-control" name="description" id="description" cols="30" rows="5" required></textarea>
                </div>

                <input class="btn btn-success" type="submit" value="Enregistrer">
            </form>
        </main>
    </div>
</div>