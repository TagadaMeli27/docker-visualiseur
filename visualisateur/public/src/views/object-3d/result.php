<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Recherche</li>
        </ol>
    </nav>
    <article>
        <div class="text-center">
            <h1>Résultats de la recherche</h1>
            <p>pour</p>
            <p class="m-auto badge bg-primary p-3 mb-3"><?= $vue['datas']['search'] ?></p>
        </div>
        <div class="d-flex flex-wrap justify-content-center">
        <!-- Ici placer tous les models issus de la recherche -->
        <?php if (!is_null($vue['datas']['object'])) { foreach ($vue['datas']['object'] as $object) { ?>
            <a href="?page=object-3d&action=listing&object=<?= $object['id'] ?>" class="display-contents text-decoration-none">
                <section class="card fit-content m-1">
                    <img class="card-img-top responsive-img" width="300" height="200" src="http://via.placeholder.com/300x200" alt="">
                    <div class="card-footer bg-primary text-white">
                        <h5 class="card-title m-0"><?= $object['nom'] ?></h5>
                    </div>
                </section>
            </a>
        <?php } } ?>
        <!-- Fin de l'affichage des models issus de la recherche -->
        </div>
    </article>
</div>