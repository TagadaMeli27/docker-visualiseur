<table>
    <thead>
        <tr>
            <th>id</th>
            <th>nom</th>
            <th>description</th>
            <th>date_creation</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($vue['datas'] as $object) { ?>
            <tr>
                <th><?= $object['id'] ?></th>
                <th><?= $object['nom'] ?></th>
                <th><?= $object['description'] ?></th>
                <th><?= $object['date_creation'] ?></th>
            </tr>
        <?php } ?>
    </tbody>
</table>