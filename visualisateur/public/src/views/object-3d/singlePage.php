<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="?page=theme-3d">Thème 3D</a></li>
            <?php /* Si [pathTheme] est défini */ if (isset($_SESSION["pathTheme"]["themeId"]) && !empty($_SESSION["pathTheme"]["themeId"])) { ?>
            <li class="breadcrumb-item"><a href="?page=theme-3d&theme=<?= $_SESSION['pathTheme']['themeId'] ?>">
                    <?= ucfirst($_SESSION['pathTheme']['themeNom']) ?>
                </a></li>
            <?php } ?>
            <?php /* Si [pathTheme] est défini */ if (isset($_SESSION["pathTheme"]['categorieId']) && !empty($_SESSION["pathTheme"]['categorieId'])) { ?>
            <li class="breadcrumb-item"><a
                    href="?page=categorie&categorie=<?= $_SESSION['pathTheme']['categorieId'] ?>">
                    <?= ucfirst($_SESSION['pathTheme']['categorieNom']) ?>
                </a></li>
            <?php } ?>
            <li class="breadcrumb-item active" aria-current="page">
                <?= ucfirst($vue['datas']['object']['nom']) ?>
            </li>
        </ol>
    </nav>
</div>
<div class="container-md mb-3">
    <article class="row">
        <div class="col-12 col-lg-6">
            <!-- fontawsome  -->
            <script src="https://kit.fontawesome.com/0b77b078c7.js" crossorigin="anonymous"></script>

            <div id="visu-wrapper" class="visu-wrapper" data-gltf="<?= $vue['datas']['object']['id'] ?>">
                <!-- ICI le visualisateur -->
                <script type="module" src="./public/assets/js/visualiseur.js"></script>
                <canvas class="visu" id="visu">
                </canvas>

                <!-- barre de progression -->
                <div class="progress">
                    <div id="progress-value">
                    </div>
                </div>
                <div class="parametre-scene">
                    <!-- intensite lumiere -->
                    <div >
                        <input id="light-slider" type="range" value="50" step="0.1" >
                        <i class="fas fa-adjust"></i>
                    </div>

                    <!-- color picker -->
                    <div >
                        <i class="fas fa-palette"></i> 
                        <input type="color" class="form-control form-control-color" id="color-input" value="#ffffff"
                        title="choisissez la couleur du modele">
                    </div>
                    

                </div>

                <!-- parametre -->
                <div class="parametre">
                    <span id="plein-ecran">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-arrows-fullscreen" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                            d="M5.828 10.172a.5.5 0 0 0-.707 0l-4.096 4.096V11.5a.5.5 0 0 0-1 0v3.975a.5.5 0 0 0 .5.5H4.5a.5.5 0 0 0 0-1H1.732l4.096-4.096a.5.5 0 0 0 0-.707zm4.344 0a.5.5 0 0 1 .707 0l4.096 4.096V11.5a.5.5 0 1 1 1 0v3.975a.5.5 0 0 1-.5.5H11.5a.5.5 0 0 1 0-1h2.768l-4.096-4.096a.5.5 0 0 1 0-.707zm0-4.344a.5.5 0 0 0 .707 0l4.096-4.096V4.5a.5.5 0 1 0 1 0V.525a.5.5 0 0 0-.5-.5H11.5a.5.5 0 0 0 0 1h2.768l-4.096 4.096a.5.5 0 0 0 0 .707zm-4.344 0a.5.5 0 0 1-.707 0L1.025 1.732V4.5a.5.5 0 0 1-1 0V.525a.5.5 0 0 1 .5-.5H4.5a.5.5 0 0 1 0 1H1.732l4.096 4.096a.5.5 0 0 1 0 .707z" />
                        </svg>
                        plein ecran   
                    </span>

                    <div class="switch-wrapper">

                        <!-- source du switch : https://codepen.io/halvves/pen/BxXNYq -->
                        <label class="label">
                            <div class="label-text">wireframe</div>
                            <div class="toggle">
                            <input id="switch-wire" class="toggle-state" type="checkbox" name="check"
                                value="check" />
                            <div class="toggle-inner">
                                    <div class="indicator"></div>
                                </div>
                                <div class="active-bg"></div>
                            </div>
                            
                        </label>

                        <label class="label">
                            <div class="label-text">masquer texture</div>
                            <div class="toggle">
                            <input id="switch-texture" class="toggle-state" type="checkbox" name="check"
                                    value="check" />
                                <div class="toggle-inner">
                                    <div class="indicator"></div>
                                </div>
                                <div class="active-bg"></div>
                            </div>
                            
                        </label>

                        <!-- wireframe
                        <label class="switch">
                            <input id="switch-wire" type="checkbox">
                            <span class="slider round"></span>
    
                        </label>
                        texture
                        <label class="switch">
                            <input id="switch-texture" type="checkbox">
                            <span class="slider round"></span>
                        </label> -->

                    </div>


                </div>


            </div>
            <aside class="d-flex justify-content-center my-3">
                <!-- button trigger helper modal -->
                <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#helper">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-info-circle-fill" viewBox="0 0 16 16">
                        <path
                            d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
                    </svg>
                </button>
                <!-- helper modal -->
                <div class="modal fade" id="helper" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-light">
                                <h4 class="modal-title">Aide à l'utilisation du visualisateur</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3">
                                    <h3 class="fs-4">Sur téléphone</h3>
                                    <p>Pour bouger l'objet, faites glisser votre doigt en appyant sur l'objet.</p>
                                </div>
                                <div>
                                    <h3 class="fs-4">Sur ordinateur</h3>
                                    <p>Pour bouger l'objet, maintenez le clic gauche de la souris enfoncé et faites
                                        glisser votre souris.</p>
                                </div>
                            </div>
                            <div class="modal-footer bg-light">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
        <div class="col-12 col-lg-6">
            <h1>
                <?= ucfirst($vue['datas']['object']['nom']) ?>
            </h1>
            <!-- Ici placer toutes les catégories et thèmes de l'objet -->
            <?php foreach ($vue['datas']['categories'] as $categorie) { ?>
            <span class="badge bg-success">
                <?= $categorie["nomCategorie"] ?>
            </span>
            <?php } ?>
            <?php foreach ($vue['datas']['themes'] as $theme) { ?>
            <span class="badge bg-primary">
                <?= $theme["nomTheme"] ?>
            </span>
            <?php } ?>
            <!-- Fin des catégories et thèmes de l'objet -->
            <p class="py-3"><?= $vue['datas']['object']['description'] ?></p>
            <p>Nombre de vues : <span class="badge bg-secondary"><?= $vue['datas']['object']['vues'] ?></span></p>
            <!-- Affichage du nombre de téléchargement -->
            <p>
                <button class="btn btn-secondary" disabled>
                    <span class="pe-2"><?= $vue['datas']['object']['telechargements'] ?></span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                    <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                    <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                    </svg>
                </button>
            </p>
            <!-- Fin affichage du nombre de téléchargement -->
            <section>
                <h2 class="fs-4">D'autres objets similaires</h2>
                <ul class="carousel">
                    <!-- Ici placer les objets "similaires" -->
                    <?php foreach($vue['datas']['recommendeObject'] as $object) { ?>
                    <li class="carousel-item">
                        <a href="?page=object-3d&object=<?= $object['id'] ?>">
                            <div class="card bg-dark text-white">
                                <img class="card-img vignette" width="300" height="200"
                                    src="./public/assets/models3d/<?= $object["id"] ?>/vignette.png" alt="vignette">
                                <div class="card-img-overlay bg-dark opacity-25"></div>
                                <div class="card-img-overlay d-flex flex-column justify-content-end align-items-center">
                                    <h5 class="card-title"> <?= $object['nom'] ?></h5>
                                </div>
                            </div>
                        </a>
                    </li>
                    <?php } ?>
                    <!-- Fin de l'affichage des objets "similaires" -->
                </ul>
            </section>
        </div>
    </article>
</div>