<a href="?page=accueil&action=administration" class="navbar-brand bg-darken col-8 col-md-3 col-lg-2 d-flex justify-content-evenly align-items-center text-dark px-2">
    <img class="responsive-img pe-1" width="50" height="50" src="https://via.placeholder.com/50" alt="Nom Entreprise">
    Nom Entreprise
</a>
<button class="navbar-toggler d-md-none" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
        </svg>
    </span>
</button>
<ul class="navbar-nav px-3 m-0">
    <li class="py-2">
        <a class="btn btn-danger" href="?page=user&action=logout">Déconnexion</a>
    </li>
</ul>