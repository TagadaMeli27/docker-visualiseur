<div class="container-fluid">
    <div class="row align-items-center px-3 pt-3">
        <img class="responsive-img col-3" src="https://via.placeholder.com/50" alt="Nom Entreprise">
        <h3 class="col m-0">Nom Entreprise</h3>
        <form class="col-sm-4 input-group search-group mt-2" action="?page=object-3d&action=search" method="post">
            <input class="form-control border-success" type="search" name="searchObject" placeholder="Recherche">
            <button class="btn btn-success" type="submit" aria-label="envoie-recherche">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>
            </button>
        </form>
    </div>
</div>
    
<nav>
    <ul class="nav-pills carousel justify-content-md-center">
        <li class="nav-item carousel-item-md">
            <a href="?page=accueil" class="nav-link <?php if ($page == 'accueil') { ?>active" aria-current="page" <?php } else echo "\""?>>Accueil</a>
        </li>
        <li class="nav-item carousel-item-md">
            <a href="?page=theme-3d" class="nav-link <?php if (($page == 'theme-3d') || ($page == 'categorie') || ($page == 'object-3d')) { ?>active" aria-current="page" <?php } else echo "\""?>>Thème 3D</a>
        </li>
        <li class="nav-item carousel-item-md">
            <a href="?page=qui-sommes-nous" class="nav-link <?php if ($page == 'qui-sommes-nous') { ?>active" aria-current="page" <?php } else echo "\""?>>Qui sommes-nous ?</a>
        </li>
        <li class="nav-item carousel-item-md">
            <a href="?page=contact" class="nav-link <?php if ($page == 'contact') { ?>active" aria-current="page" <?php } else echo "\""?>>Contact</a>
        </li>
    </ul>
</nav>