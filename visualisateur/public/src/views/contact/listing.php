<div class="container-md">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">Contact</li>
        </ol>
    </nav>
    <section>
        <h1>Nous contacter</h1>
        <?php if (isset($vue['alert']) && !empty($vue['alert']) && !is_null($vue['alert'])) { ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </svg>
            <?= $vue['alert'] ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <?php } ?>
        <form action="" method="post">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="email">Votre e-mail :</label>
                            <input class="form-control" type="email" name="mail" id="email" placeholder="example@example.com">
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="objet">Objet :</label>
                            <input class="form-control" type="text" name="object" id="objet" placeholder="sujet du message">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="message">Votre message :</label>
                            <textarea class="form-control" name="message" id="message" rows="5" placeholder="Je vous écris ..."></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center my-3">
                <input class="btn btn-primary" type="submit" value="Envoyer">
            </div>
        </form>
        <p>Téléphone : <a href="tel:+33232454545">02 32 45 45 45</a></p>
    </section>
</div>