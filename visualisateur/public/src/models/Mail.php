<?php
class Mail
{
    private string $mail_adress;
    private bool $etat;
    private string $debug;

    public function __construct(string $mail)
    {
        $this->mail_adress = $mail;
        $this->etat = false;
        $this->debug = "";
    }

    public function etat()
    {
        return $this->etat;
    }

    public function debug()
    {
        return $this->debug;
    }

    public function send($mail, $objet, $message)
    {
        if (!empty($objet) && !is_null($objet) && !empty($message) && !is_null($message) && !empty($mail) && !is_null($mail))
        {
            $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
            $mail = filter_var($mail, FILTER_VALIDATE_EMAIL);

            if (!$mail)
            {
                $this->etat = false;
                $this->debug = "Veuillez saisir une adresse mail valide !";
            }

            $objet = filter_var($objet, FILTER_SANITIZE_STRING);
            $message = filter_var($message, FILTER_SANITIZE_STRING);

            $message = str_replace("\n.", "\n..", $message); // Sur windows uniquement, éviter que php supprime les points en début de lignes

            // Composition des headers du mail
            $headers = array(
                "MIME-Version" => "1.0",
                "Content-type" => "text/plain; charset=utf-8",
                "From" => $mail,
                "X-Mailer" => "PHP/".phpversion(),
            );

            $retour = mail($this->mail_adress, $objet, $message, $headers);

            // Vérification des potentielles erreurs
            if ($retour)
            {
                $this->etat = true;
                $this->debug = "Message envoyé !";
            }
            else
            {
                $this->etat = false;
                $this->debug = "Erreur, message non envoyé !";
            }
        }
        else
        {
            $this->etat = false;
            $this->debug = "Veuillez remplir les différents champs du formulaire !";
        }
    }
}