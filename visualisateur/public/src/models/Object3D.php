<?php

require_once "./public/src/models/Model.php";

class Object3D extends Model{
    public function __construct()
    {
        parent::__construct("3d_object");
    }

    public function insert(array $datas)
    {
        $sql = "insert into 3d_object(nom, description, date_creation) values(:nom, :description, :date_creation);";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $datas['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $datas['description'], PDO::PARAM_STR, 255);
        $request->bindParam(":date_creation", $datas['date_creation'], PDO::PARAM_STR, 255);
        $request->execute();
        return $this->bdd->lastInsertId();
    }

    public function update(int $id, array $datas)
    {
        $sql = "update 3d_object set nom = :nom, description = :description where id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $datas['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $datas['description'], PDO::PARAM_STR, 255);
        $request->bindParam(":id", $id, PDO::PARAM_INT);
        $request->execute();
    }

    public function updateVue(int $idObject, int $vues)
    {
        $sql = "update 3d_object set vues = :vues where id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(':vues', $vues, PDO::PARAM_INT);
        $request->bindParam(':id', $idObject, PDO::PARAM_INT);
        $request->execute();
    }

    public function getObjectByTheme(int $id_theme)
    {
        $sql = "select distinct 3d_object.id as idObject, 3d_object.vues as vuesObject, 3d_object.telechargements as telechargementsObject, 3d_object.nom as nomObject, 3d_object.description as descriptionObject, 3d_object.date_creation as creationObject, categorie.id as idCategorie, categorie.nom as nomCategorie, categorie.description as descriptionCategorie, categorie.date_creation as creationCategorie, theme.id as idTheme, theme.nom as nomTheme, theme.description as descriptionTheme, theme.date_creation as creationTheme from 3d_object "
            . "inner join categorie_object on 3d_object.id = categorie_object.id_object "
            . "inner join categorie on categorie_object.id_categorie = categorie.id "
            . "inner join theme_categorie on categorie.id = theme_categorie.id_categorie "
            . "inner join theme on theme_categorie.id_theme = theme.id "
            . "where theme.id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_theme, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getObjectByCategorie(int $id_categorie)
    {
        $sql = "select distinct 3d_object.id as idObject, 3d_object.vues as vuesObject, 3d_object.telechargements as telechargementsObject, 3d_object.nom as nomObject, 3d_object.description as descriptionObject, 3d_object.date_creation as creationObject from 3d_object "
            . "inner join categorie_object on 3d_object.id = categorie_object.id_object "
            . "inner join categorie on categorie_object.id_categorie = categorie.id "
            . "where categorie.id = :id;";
        $request = $this->bdd->prepare($sql);
        $request->bindParam(":id", $id_categorie, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    } 
    
    public function getObjectAcceuil()
    {
        $sql = "select * from 3d_object order by date_creation limit 15;";
        $request = $this->bdd->prepare($sql);

        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getObjectRecommende()
    {
        $sql = "select * from 3d_object order by vues limit 15;";
        $request = $this->bdd->prepare($sql);

        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSimilaire(int $idObject)
    {
        $sql = "select distinct 3d_object.*, categorie_object.id_categorie from 3d_object "
            . "inner join categorie_object on categorie_object.id_object = 3d_object.id "
            . "where categorie_object.id_categorie = "
            . "(select categorie_object.id_categorie from categorie_object inner join 3d_object on categorie_object.id_object = 3d_object.id where id = :id limit 1) "
            . "and 3d_object.id != :id "
            . "limit 10;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $idObject, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function search(string $key)
    {
        $sql = "select * from 3d_object where nom like :key or description like :key order by date_creation;";
        $request = $this->bdd->prepare($sql);

        $key = "%".$key."%";
        $request->bindParam(":key", $key, PDO::PARAM_STR, 255);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getChildObject($categorie)
    {
        $sql = "select distinct 3d_object.* from 3d_object "
            . "inner join categorie_object on 3d_object.id = categorie_object.id_object "
            . "where categorie_object.id_categorie = :id";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $categorie, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
}