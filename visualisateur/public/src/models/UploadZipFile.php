<?php
/** 
 * === CONFIGURATION PHP INIT ===
 * Avant d'utiliser ce script, penser à vérifier dans php.ini :
 * - post_max_size
 * - upload_max_filesize
 * => leurs valeurs doivent être suppérieures à la taille des fichiers à upload !
 * => config actuelle : 400M
 */

class UploadZipFile
{
    private array $tabExtension = array('zip');
    private string $extension = "";
    private string $message = "";
    private string $name;
    private array $files;
    private string $slug;
    private string $target;

    public function __construct(string $name, array $files, string $slug, string $target = "./")
    {
        $this->name = $name;
        $this->files = $files;
        $this->slug = $slug;
        $this->target = $target;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function debugFiles()
    {
        print_r($this->files);
    }

    public function upload()
    {
        // Récuperation de l'extension du fichier
        if (!empty($this->files[$this->name]['name']))
        {
            $this->extension = pathinfo($this->files[$this->name]['name'], PATHINFO_EXTENSION);

            // Vérification de l'extension du fichier
            if (in_array( strtolower($this->extension), $this->tabExtension) )
            {
                // Parcours du tableau d'erreurs
                if(isset($this->files[$this->name]['error']) && UPLOAD_ERR_OK === $this->files[$this->name]['error'])
                {
                    // On renomme le fichier
                    $nom = $this->slug.".".$this->extension;
                    
                    // Si c'est OK, on teste l'upload
                    if(move_uploaded_file($this->files[$this->name]['tmp_name'], $this->target.$nom))
                        $this->message = "Upload réussi !";
                    else
                        $this->message = "Problème lors de l'upload !";
                }
                else
                   $this->message = $this->getUploadErrors($this->files[$this->name]['error']);
            }
            else
                $this->message = "Veuillez fournir un fichier .zip !";
        }
        else
            $this->message = 'Veuillez remplir le formulaire svp !';
    }

    private function getUploadErrors($error)
    {
        $message = 'Une erreur interne a empêché l\'uplaod.';

        // Message d'erreur plus précis
        switch ($error) 
        {
            case 1:
                $message .= " Votre fichier est trop lourd par rapport à la configuration.";
                break;
                                
            case 2:
                $message .= " Votre fichier est trop lourd par rapport à la taille autorisé dans le formulaire.";
                break;    
                                
            case 3:
                $message .= " Votre fichier n'a été que partiellement téléchargé.";
                break;
                                
            case 4:
                $message .= " Votre fichier n'a pas été téléchargé.";
                break;
                                
            case 6:
                $message .= " Un dossier temporaire est manquant.";
                break;
                                
            case 7:
                $message .= " Echec de l'écriture du fichier sur le disque.";
                break;
                            
            default:
                break;
        }

        return $message;
    }
}