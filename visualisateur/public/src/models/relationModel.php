<?php

require_once "./public/src/models/BddModel.php";
require_once "./public/src/models/Model.php";

/**
 * Classe qui permet de gérer les relations à la base de données
 */

class RelationModel extends Model{
    public function __construct()
    {
        parent::__construct("aucune importance"); 
    }

    /**
     * Methode qui supprime toute les relation du theme dont l'id est passé en parametre
     * et qui ajoute toutes les categories du tableau id_categorie constitué de string
     */

    public function setThemeCategorie(int $id_theme, array $id_categorie)
    {
        $sql = "delete from theme_categorie where id_theme = :theme";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":theme", $id_theme, PDO::PARAM_INT);
        $request->execute();

        foreach ($id_categorie as $id) {
            unset($sql);
            unset($request);
            $categorieId = intval($id);

            $sql = "insert into theme_categorie(id_theme, id_categorie) values(:theme, :categorie);";
            $request = $this->bdd->prepare($sql);
    
            $request->bindParam(":theme", $id_theme, PDO::PARAM_INT);
            $request->bindParam(":categorie", $categorieId, PDO::PARAM_INT);
            $request->execute();
        }
    }

    public function setCategorieObject(int $categorie, array $object)
    {
        $sql = "delete from categorie_object where id_categorie = :categorie;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":categorie", $categorie, PDO::PARAM_INT);
        $request->execute();

        foreach ($object as $id) {
            unset($sql);
            unset($request);
            $objectId = intval($id);

            $sql = "insert into categorie_object(id_categorie, id_object) values(:categorie, :object);";
            $request = $this->bdd->prepare($sql);
    
            $request->bindParam(":categorie", $categorie, PDO::PARAM_INT);
            $request->bindParam(":object", $objectId, PDO::PARAM_INT);
            $request->execute();
        }
    }

    public function setCategorieCategorie(int $id_parent, array $enfant)
    {
        $sql = "delete from categorie_categorie where id_parent = :parent;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":parent", $id_parent, PDO::PARAM_INT);
        $request->execute();

        foreach ($enfant as $id) {
            unset($sql);
            unset($request);
            $enfantId = intval($id);

            $sql = "insert into categorie_categorie(id_parent, id_enfant) values(:id_parent, :id_enfant);";
            $request = $this->bdd->prepare($sql);

            $request->bindParam(":id_parent", $id_parent, PDO::PARAM_INT);
            $request->bindParam(":id_enfant", $enfantId, PDO::PARAM_INT);
            $request->execute();
        }
    }

    public function getRelationThemeCategorie(int $theme)
    {
        $sql = "select * from theme_categeorie where id_theme = :theme;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":theme", $theme, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getRelationCategorieObject(int $categorie)
    {
        $sql = "select * from categorie_object where id_categorie = :categorie;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":categorie", $categorie, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getRelationCategorieCategorie(int $categorieParent)
    {
        $sql = "select * from categorie_categorie where id_parent = :categorieParent;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":categorie", $categorieParent, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
}