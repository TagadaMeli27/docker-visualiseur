<?php

require_once "./public/src/models/Model.php";

class Theme extends Model{
    public function __construct()
    {
        parent::__construct("theme");
    }
    public function selectWithParams(array $params)
    {
        $sql = "select * from theme where nom = :nom and description = :description;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $params['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $params['description'], PDO::PARAM_STR, 255);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
    public function insert(array $params)
    {
        $sql = "insert into theme(nom, description, date_creation) value(:nom, :description, :date_creation);";
        $request = $this->bdd->prepare($sql);
        
        $request->bindParam(":nom", $params['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $params['description'], PDO::PARAM_STR, 255);
        $request->bindParam(":date_creation", $params['date_creation'], PDO::PARAM_STR, 255);
        $request->execute();
    }

    public function update(int $id, array $datas)
    {
        $sql = "update theme set nom = :nom, description = :description where id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $datas['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $datas['description'], PDO::PARAM_STR, 255);
        $request->bindParam(":id", $id, PDO::PARAM_INT);
        $request->execute();
    }

    public function getThemeByObject(int $id_object)
    {
        $sql = "select distinct theme.id as idTheme, theme.nom as nomTheme from theme "
            . "inner join theme_categorie on theme.id = theme_categorie.id_theme "
            . "inner join categorie_object on theme_categorie.id_categorie = categorie_object.id_categorie "
            . "inner join 3d_object on categorie_object.id_object = 3d_object.id "
            . "where 3d_object.id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_object, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getThemeByCategorie(int $id_categorie)
    {
        $sql = "select distinct theme.id as idTheme, theme.nom as nomTheme from theme "
            . "inner join theme_categorie on theme.id = theme_categorie.id_theme "
            . "inner join categorie on theme_categorie.id_categorie = categorie.id "
            . "where categorie.id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_categorie, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getThemeAcceuil()
    {
        $sql = "select * from theme order by date_creation limit 15;";
        $request = $this->bdd->prepare($sql);

        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCategorie(int $id)
    {
        $sql = "select categorie.* from categorie "
            . "inner join theme_categorie on categorie.id = theme_categorie.id_categorie "
            . "where theme_categorie.id_theme = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
}