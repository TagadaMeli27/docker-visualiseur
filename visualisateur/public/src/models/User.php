<?php

require_once "./public/src/models/Model.php";

class User extends Model{
    public function __construct()
    {
        parent::__construct("utilisateur");
    }

    public function Exist(string $mail)
    {
        $sql = "select * from utilisateur where mail = :mail;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":mail", $mail, PDO::PARAM_STR, 255);
        $request->execute();

        if ($request->rowCount() == 0) {
            return false;
        }
        else {
            return true;
        }
    }

    public function listing(string $mail = null)
    {
        if ($mail != null) {
            $sql = "select mail, role, date_creation, mdp from utilisateur where mail = :mail";
            $request = $this->bdd->prepare($sql);

            $request->bindParam(":mail", $mail, PDO::PARAM_STR, 100);
        }
        else {
            $sql = "select mail, role, date_creation, mdp from utilisateur";
            $request = $this->bdd->prepare($sql);
        }

        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update(string $mail, string $role, string $mdp)
    {
        $sql = "update utilisateur set mail = :mail, role = :role, mdp = :mdp;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":mail", $mail, PDO::PARAM_STR, 100);
        $request->bindParam(":role", $role, PDO::PARAM_STR, 100);
        $request->bindParam(":mdp", $mdp, PDO::PARAM_STR, 100);
        $request->execute();
    }

    public function remove(string $mail)
    {
        $sql = "delete from utilisateur where mail = :mail;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":mail", $mail, PDO::PARAM_STR, 100);
        $request->execute();
    }

    public function add(string $mail, string $role, string $mdp, string $date_creation)
    {
        $sql = "insert into utilisateur(mail, role, mdp, date_creation) values(:mail, :role, :mdp, :date_creation);";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":mail", $mail, PDO::PARAM_STR, 100);
        $request->bindParam(":role", $role, PDO::PARAM_STR, 100);
        $request->bindParam(":mdp", $mdp, PDO::PARAM_STR, 255);
        $request->bindParam(":date_creation", $date_creation, PDO::PARAM_STR, 100);
        $request->execute();
    }
}