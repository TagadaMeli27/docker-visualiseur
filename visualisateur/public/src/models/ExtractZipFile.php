<?php
class ExtractZipFile
{
    private ZipArchive $zip;

    public function __construct()
    {
        $this->zip = new ZipArchive;
    }

    public function extractZip(string $slug, string $destination)
    {
        // Fichier zip à décompresser
        $path = "$destination$slug.zip";

        // Ouvrir le fichier zip
        $bool = $this->zip->open($path);
        if ($bool === true)
        {
            // Avant de décompresser le zip on veut retrouver le nom du dossier parent et du fichier gltf pour les renommer
            $find_dir = false;
            $find_gltf = false;
            $find_img = false;

            for ($i = 0; $i < $this->zip->numFiles; $i++)
            {
                $entry = $this->zip->getNameIndex($i); // Nom du fichier ou dossier courant
                
                // Si c'est un dossier
                if (substr( $entry, -1 ) == '/' && $entry != "textures/" && !$find_dir)
                {
                    $dirname = $entry;
                    $find_dir = true;
                    if ($find_gltf == true) // Si les 2 sont trouvés, on arrête la boucle
                        break;
                    continue;
                }

                // Si c'est un fichier .gltf
                if ((str_contains($entry, ".gltf") || str_contains($entry, ".glb")) && !$find_gltf)
                {
                    $gltf_name = $entry;
                    $find_gltf = true;
                    if ($find_dir == true) // Si les 2 sont trouvés, on arrête la boucle
                        break;
                    continue;
                }
            }

            // Renommage
            if (isset($dirname))
            {
                // Décompression de l'archive et fermeture du fichier zip
                $this->zip->extractTo($destination);
                $this->zip->close();
                
                // Renommage du fichier .gltf
                if (isset($gltf_name))
                {
                    $old_gltf_name = $destination."/".$gltf_name;
                    if (str_contains($entry, ".gltf"))
                        $new_gltf_name = $destination."/".$dirname.$slug.".gltf";
                    else
                        $new_gltf_name = $destination."/".$dirname.$slug.".glb";
                    rename($old_gltf_name, $new_gltf_name);
                }

                // Renommage du dossier parent
                $old_dirname = $destination."/".$dirname;
                $new_dirname = $destination."/".$slug."/";
                rename($old_dirname, $new_dirname);
            }
            else
            {
                // On crée un dossier racine
                mkdir($destination.$slug);

                // On update la destination d'extraction
                $destination.= $slug;
                
                // Décompression de l'archive et fermeture du fichier zip
                $this->zip->extractTo($destination);
                $this->zip->close();

                // On rename le fichier .gltf
                if (isset($gltf_name))
                {
                    $old_gltf_name = $destination."/".$gltf_name;
                    if (str_contains($entry, ".gltf"))
                        $new_gltf_name = $destination."/".$slug.".gltf";
                    else
                        $new_gltf_name = $destination."/".$slug.".glb";
                    rename($old_gltf_name, $new_gltf_name);
                }
            }
        }
        else
        {
            $message = "Problème avec l'ouverture de l'archive, code erreur : ".$res;
        }
    }

    public function deleteZip(string $slug, string $destination)
    {
        // Fichier zip à supprimer
        $path = "$destination/$slug.zip";
        unlink($path);
    }

    public function deleteFolder(string $slug, string $destination)
    {
        // Dossier à supprimer
        $path = "$destination$slug/";
        
        if (file_exists($path)) 
            $this->deleteSubDirectoryOrFile($path);
        else
        {
            if (is_dir($path))
                rmdir($path);
        }
    }

    private function deleteSubDirectoryOrFile($name)
    {
        if (file_exists($name)) 
        {
            $dir = opendir($name);
            while (false !== ($file = readdir($dir))) 
            {
                if (($file != '.') && ($file != '..')) 
                {
                    $full = $name.$file;
                    echo $full."<br/>";
                    if (is_dir($full))
                        $this->deleteSubDirectoryOrFile($full."/");
                    else
                        unlink($full);
                }
            }
            closedir($dir);
            rmdir($name);
        }
        else
            rmdir($name);
    }
}