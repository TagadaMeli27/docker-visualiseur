<?php
/**
 * cette classe permet de retourner toujours la même connexion à la base de données
 * Elle s'intencie elle même et permet d'obtimiser la connection à la base de données
 */

class BddModel{
    private $PDOInstance = null;
    private static $instance = null;

    private function __construct()
    {
        // instanciation de l'attribue pdo
        $this->PDOInstance = new PDO("mysql:host=mariadb;port=3306;dbname=visualiseur", "root", "");
        $this->PDOInstance->exec('SET NAMES utf8');
    }

    /**
     * methode static qui retourne l'attribue instance qui contient BddModel déja instancier
     * si elle est vide, elle instancie la classe et retourne la class einstancier
     */
    public static function getInstance()
    {
        if (self::$instance == null){
            self::$instance = new BddModel();
        }
        return self::$instance;
    }
    /**
     * retourn la connexion à la base de donnée
     */
    public function getConnection(){
        return $this->PDOInstance;
    }
}