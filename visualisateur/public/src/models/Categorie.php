<?php

require_once "./public/src/models/Model.php";

class Categorie extends Model{
    public function __construct()
    {
        parent::__construct('categorie');   
    }
    public function selectWithParams($params){
        $sql = "select * from categorie where nom = :nom and description = :description;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $params['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $params['description'], PDO::PARAM_STR, 255);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
    public function insert(array $datas)
    {
        $sql = "insert into categorie(nom, description, date_creation) values(:nom, :description, :date_creation);";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $datas['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $datas['description'], PDO::PARAM_STR, 255);
        $request->bindParam(":date_creation", $datas['date_creation'], PDO::PARAM_STR, 255);
        $request->execute();
    }

    public function update(int $id, array $datas)
    {
        $sql = "update categorie set nom = :nom, description = :description where id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":nom", $datas['nom'], PDO::PARAM_STR, 255);
        $request->bindParam(":description", $datas['description'], PDO::PARAM_STR, 255);
        $request->bindParam(":id", $id, PDO::PARAM_INT);
        $request->execute();
    }

    public function getCategorieByTheme(int $id_theme)
    {
        $sql = "select distinct categorie.id as idCategorie, categorie.nom as nomCategorie from categorie "
            . "inner join theme_categorie on categorie.id = theme_categorie.id_categorie "
            . "inner join theme on theme_categorie.id_theme = theme.id "
            . "where theme.id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_theme, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCategorieByObject(int $id_object)
    {
        $sql = "select distinct categorie.id as idCategorie, categorie.nom as nomCategorie from categorie "
            . "inner join categorie_object on categorie.id = categorie_object.id_categorie "
            . "inner join 3d_object on categorie_object.id_object = 3d_object.id "
            . "where 3d_object.id = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_object, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getParentCategorie(int $id_categorie)
    {
        $sql = "select distinct categorie.id as idCategorie, categorie.nom as nomCategorie from categorie "
            . "inner join categorie_categorie on categorie.id = categorie_categorie.id_parent "
            . "where categorie_categorie.id_enfant = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_categorie, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getChildCategorie(int $id_categorie)
    {
        $sql = "select distinct categorie.id as idCategorie, categorie.nom as nomCategorie from categorie "
            . "inner join categorie_categorie on categorie.id = categorie_categorie.id_enfant "
            . "where categorie_categorie.id_parent = :id;";
        $request = $this->bdd->prepare($sql);

        $request->bindParam(":id", $id_categorie, PDO::PARAM_INT);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }
}