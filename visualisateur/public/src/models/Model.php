<?php

require_once "./public/src/models/BddModel.php";

abstract class Model{
    protected string $table;
    protected $bdd;

    protected function __construct(string $uneTable)
    {
        $this->table = $uneTable;
        $this->bdd = BddModel::getInstance()->getConnection();
    }

    public function select(int $id = null){
        if (!is_null($id)) {
            $sql = "select * from " . $this->table . " where id = :id";
            $request = $this->bdd->prepare($sql);
            $request->bindParam(':id', $id, PDO::PARAM_INT);
        }
        else {
            $sql = "select * from " . $this->table . ";";
            $request = $this->bdd->prepare($sql);
        }

        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete(int $id)
    {
        $sql = "delete from " . $this->table . " where id = :ID";
        $request = $this->bdd->prepare($sql);
        $request->bindParam(':ID', $id, PDO::PARAM_INT);
        $request->execute();
    }
}