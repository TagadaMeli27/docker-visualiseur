<?php

session_start();

function getCurrentUrl() {
    $host = $_SERVER["HTTP_HOST"];
    $url = $_SERVER["PHP_SELF"];
    $prompt = explode("/", $url);
    
    if(end($prompt) != "" && str_starts_with(end($prompt), "index")) {
        array_pop($prompt);
    }
    
    $uri = "";

    for($i = 0; $i < count($prompt); $i++) {
        $uri .= $prompt[$i] . "/";
    }

    return $host . $uri;
}

function getCurrentPath() {
    $url = $_SERVER["REQUEST_URI"];
    $prompt = explode("/", $url);
    
    if(end($prompt) != "" && str_starts_with(end($prompt), "index")) {
        array_pop($prompt);
    }

    $uri = "";

    for($i = 0; $i < count($prompt); $i++) {
        $uri .= $prompt[$i] . "/";
    }

    return $uri;
}

$pages = [  "accueil" => AccueilController::class,
            "theme-3d" => ThemeController::class,
            "categorie" => CategorieController::class,
            "object-3d" => Object3DController::class,
            "user" => UtilisateurController::class,
            "qui-sommes-nous" => QuiSommesNousController::class,
            "contact" => ContactController::class,
        ];
$page = "accueil";
$action = "listing";

if (isset($_GET['page']) && !is_null($_GET['page']) && !empty($_GET['page']))
{
    $page = $_GET['page'];
}
if (isset($_GET['action']) && !is_null($_GET['action']) && !empty($_GET['action']))
{
    $action = $_GET['action'];
}

if (array_key_exists($page, $pages))
{
    require_once "./public/src/controllers/" . $pages[$page] . ".php";

    $controller = new $pages[$page]();
    $vue = $controller->{$action}();

    if (isset($vue["administration"]) && !is_null($vue["administration"]) && $vue["administration"])
        require_once "./public/src/views/baseDashboard.php";
    else
        require_once "./public/src/views/base.php";
}
else
{
    require_once "./public/src/views/404.php";
}