drop database if exists visualiseur;
CREATE DATABASE visualiseur CHARACTER SET utf8 COLLATE utf8_general_ci;
use visualiseur;

drop table if exists utilisateur;
create table utilisateur(
    mail varchar(100) primary key not null,
    role varchar(50) default "user",
    mdp varchar(100) not null,
    date_creation datetime not null
);

drop table if exists 3d_object;
create table 3d_object(
    id int(255) primary key auto_increment not null,
    nom varchar(100) not null,
    description text(2000) null,
    date_creation datetime not null,
    vues int(255) default 0,
    telechargements int(255) default 0
);

drop table if exists categorie;
create table categorie(
    id int(255) primary key auto_increment not null,
    nom varchar(100) not null,
    description text(2000) null,
    date_creation datetime not null
);

drop table if exists theme;
create table theme(
    id int(255) primary key auto_increment not null,
    nom varchar(100) not null,
    description text(2000) null,
    date_creation datetime not null
);

drop table if exists theme_categorie;
create table theme_categorie(
    id_theme int(255),
    id_categorie int(255)
);
drop table if exists categorie_categorie;
create table categorie_categorie(
    id_parent int(255),
    id_enfant int(255)
);
drop table if exists categorie_object;
create table categorie_object(
    id_categorie int(255),
    id_object int(255)
);

alter table theme_categorie
add foreign key (id_theme) references theme(id)
on delete CASCADE;

alter table categorie_categorie
add foreign key (id_parent) references categorie(id)
on delete CASCADE;

alter table categorie_categorie
add foreign key (id_enfant) references categorie(id)
on delete CASCADE;

alter table theme_categorie
add foreign key (id_categorie) references categorie(id)
on delete CASCADE;

alter table categorie_object
add foreign key (id_categorie) references categorie(id)
on delete CASCADE;

alter table categorie_object
add foreign key (id_object) references 3d_object(id)
on delete CASCADE;

delete from 3d_object;
delete from theme;
delete from categorie;
-- insert dans la table 3d_object

insert into 3d_object(nom, description, date_creation) values("Steampunk", "Drole de machine futuriste du passé", NOW());
insert into 3d_object(nom, description, date_creation) values("étagère", "meuble murale de rangement d'interieure en bois", NOW());
insert into 3d_object(nom, description, date_creation) values("voiture", "machine permettant de ce déplacer dans la vie quotidienne sur de courte ou longue distances", NOW());
insert into 3d_object(nom, description, date_creation) values("chien", "animal de compagnie considéré comme étant le meilleur ami de l'homme", NOW());
insert into 3d_object(nom, description, date_creation) values("chat", "animal de compagnie considéré comme étant le plus ingrat de tous", NOW());
insert into 3d_object(nom, description, date_creation) values("train", "Véhicule de transport énorme nécessiant la pose de rail avant utilisation", NOW());

-- insert dans la table categorie

insert into categorie(nom, description, date_creation) values("rangement", "tout les meuble et utilitaire de rangement", NOW());
insert into categorie(nom, description, date_creation) values("véhicule", "tout les moyens de transports existant", NOW());
insert into categorie(nom, description, date_creation) values("transports en commun", "tout les moyens de transports pen communs", NOW());
insert into categorie(nom, description, date_creation) values("animal", "être vivant non-humain et non-végétale", NOW());

-- insert dans la table theme

insert into theme(nom, description, date_creation) values("maison", "tout les objets présent dans les maisons", NOW());
insert into theme(nom, description, date_creation) values("être vivant", "tout les être vivants", NOW());
insert into theme(nom, description, date_creation) values("transports", "tout les transports", NOW());

-- insert dans la table categorie_object

insert into categorie_object(id_categorie, id_object) values(1, 2);
insert into categorie_object(id_categorie, id_object) values(2, 1);
insert into categorie_object(id_categorie, id_object) values(2, 3);
insert into categorie_object(id_categorie, id_object) values(4, 4);
insert into categorie_object(id_categorie, id_object) values(4, 5);
insert into categorie_object(id_categorie, id_object) values(1, 6);
insert into categorie_object(id_categorie, id_object) values(2, 6);

-- insert dans la table theme_categorie

insert into theme_categorie(id_theme, id_categorie) values(1, 1);
insert into theme_categorie(id_theme, id_categorie) values(3, 2);
insert into theme_categorie(id_theme, id_categorie) values(3, 3);
insert into theme_categorie(id_theme, id_categorie) values(2, 4);

-- insert dans la table categorie_categorie

insert into categorie_categorie(id_parent, id_enfant) values(2, 3);

-- insert dans la table utilisateur
insert into utilisateur(mail, role, mdp, date_creation) values("admin@gmail.com", "admin", "$2y$10$2ZR4w7s0FpK0AWm7EEfEfucuUXTezMPl.ls4UJfizRs7mFtmWfQg2", NOW());

